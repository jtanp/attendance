package fi.vamk.e1700807.attendance;

import fi.vamk.e1700807.attendance.entity.Attendance;
import fi.vamk.e1700807.attendance.repository.AttendanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
public class AttendanceApplication {

	@Autowired
	private AttendanceRepository attendanceRepository;

	public static void main(String[] args) {
		SpringApplication.run(AttendanceApplication.class, args);
	}

	@Bean
	public void initData() throws Exception {
		Attendance attendance1 = new Attendance("r5b6", new Date());
		Attendance attendance2 = new Attendance("tpwte", new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse("10.04.2021 12:30:00"));
		attendanceRepository.save(attendance1);
		attendanceRepository.save(attendance2);
	}

}
