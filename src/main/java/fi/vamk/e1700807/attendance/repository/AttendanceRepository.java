package fi.vamk.e1700807.attendance.repository;

import fi.vamk.e1700807.attendance.entity.Attendance;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {
    public Attendance findByKey(String key);
    public Attendance findByDate(Date date);
}
