package fi.vamk.e1700807.attendance.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class Controller {

    @RequestMapping("/test")
    public String test() {
        return "{\"id\":1}";
    }

}
