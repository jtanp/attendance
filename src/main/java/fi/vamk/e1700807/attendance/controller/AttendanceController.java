package fi.vamk.e1700807.attendance.controller;

import fi.vamk.e1700807.attendance.entity.Attendance;
import fi.vamk.e1700807.attendance.repository.AttendanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class AttendanceController {

    @Autowired
    private AttendanceRepository attendanceRepository;

    @GetMapping("/attendances")
    public Iterable<Attendance> getAttendances() {
        return attendanceRepository.findAll();
    }

    @GetMapping("/attendance/{id}")
    public Optional<Attendance> getAttendanceById(@PathVariable("id") int id) {
        return attendanceRepository.findById(id);
    }

    @PostMapping("/attendance")
    public @ResponseBody Attendance createAttendance(@RequestBody Attendance attendance) {
        if (!(attendance.getDate() == null)) {
            return attendanceRepository.save(attendance);
        }
        return null;
    }

    @PutMapping("/attendance")
    public @ResponseBody Attendance updateAttendance(@RequestBody Attendance attendance) {
        if (!(attendance.getDate() == null)) {
            return attendanceRepository.save(attendance);
        }
        return null;
    }

    @DeleteMapping("/attendance")
    public void deleteAttendance(@RequestBody Attendance attendance) {
        attendanceRepository.delete(attendance);
    }

}
