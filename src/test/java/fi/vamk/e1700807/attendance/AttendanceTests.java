package fi.vamk.e1700807.attendance;

import fi.vamk.e1700807.attendance.entity.Attendance;
import fi.vamk.e1700807.attendance.repository.AttendanceRepository;
import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = { "fi.vamk.e1700807.attendance" })
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)
public class AttendanceTests {

    @Autowired
    private AttendanceRepository attendanceRepository;

    @Test
    public void postGetDeleteAttendance() {
        // Attendance count at the beginning
        Iterable<Attendance> countAtBeginning = attendanceRepository.findAll();
        System.out.println("Count at the beginning: " + IterableUtils.size(countAtBeginning));
        // POST test
        Attendance attendance = new Attendance("test", new Date());
        System.out.println("Before saving: " + attendance.toString());

        Attendance saved = attendanceRepository.save(attendance);
        System.out.println("After saving: " + saved.toString());
        // Find by key test
        Attendance found = attendanceRepository.findByKey(attendance.getKey());
        System.out.println("Found: " + found.toString());
        // Compare keys
        assertThat(found.getKey().equalsIgnoreCase(attendance.getKey()));
        // Delete test
        attendanceRepository.delete(found);
        // Attendance count at the end
        Iterable<Attendance> countAtEnd = attendanceRepository.findAll();
        System.out.println("Count at the end: " + IterableUtils.size(countAtEnd));
        // Comparing size
        assertEquals((long) IterableUtils.size(countAtBeginning), (long) IterableUtils.size(countAtEnd));
    }

    @Test
    public void compareAttendanceDates() {
        System.out.println("Comparing dates of two attendances");
        // POST test
        Attendance attendance = new Attendance("123", new Date());
        System.out.println("Before saving: " + attendance.toString());
        attendanceRepository.save(attendance);
        // Find by date test
        Attendance found = attendanceRepository.findByDate(attendance.getDate());
        System.out.println("Found: " + found.toString());
        // Compare dates
        assertThat(found.toString().equalsIgnoreCase(attendance.toString()));
        System.out.println("");
    }
}
