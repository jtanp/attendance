package fi.vamk.e1700807.attendance;

import fi.vamk.e1700807.attendance.entity.Attendance;
import fi.vamk.e1700807.attendance.repository.AttendanceRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class AttendanceControllerTests extends AbstractTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private AttendanceRepository attendanceRepository;

    @WithMockUser("USER")
    @Test
    public void getAttendaces() throws Exception {
        Attendance attendance = attendanceRepository.save(new Attendance("c6b31a", new Date()));

        MvcResult mvcResult = mvc.perform(
                MockMvcRequestBuilders.get("/attendance/" + attendance.getId()).accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(super.mapToJson(attendance), content);
    }

    @WithMockUser("USER")
    @Test
    public void createAttendance() throws Exception {
        Attendance attendance = new Attendance("gtr4f1", new Date());
        attendance.setId(3);
        String inputJson = super.mapToJson(attendance);

        System.out.println(inputJson);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/attendance")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(super.mapToJson(attendance), content);
    }
}
